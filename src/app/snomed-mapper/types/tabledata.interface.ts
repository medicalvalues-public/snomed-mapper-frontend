export enum RowStatus {
  UNSET = 'unset',
  SUGGESTION = 'suggested',
  MAPPED = 'mapped',
  SKIPPED = 'skipped',
}

export interface SnomedSuggestion {
  snomed: string;
  description: string;
}

export interface TableRow {
  rowIndex: number;
  values: string[];
  status: RowStatus;
  snomed: string | undefined;
  snomedDescription: string | undefined;
  suggestions?: SnomedSuggestion[];
}

export interface TableColumn {
  header: string | undefined;
  containsDescription: boolean;
}

export interface TableData {
  tableId: string;
  filename: string;
  headersIncluded: boolean;
  language: string;
  rowCount: number;
  columns: TableColumn[];
  rows: TableRow[];
}
