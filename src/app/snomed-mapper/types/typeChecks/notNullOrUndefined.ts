export function notNullOrUndefined<T>(o: T | null | undefined): o is T {
  return o !== null && o !== undefined;
}
