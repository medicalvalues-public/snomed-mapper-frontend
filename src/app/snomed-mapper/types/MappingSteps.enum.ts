export enum MappingStep {
  CSV_UPLOAD = 'csv-upload',
  SNOMED_MAPPING = 'snomed-mapping',
  COLUMN_SELECTION = 'column-selection',
  CSV_DOWNLOAD = 'csv-download',
}

export interface StepConfig {
  page: MappingStep;
  description: string;
}
