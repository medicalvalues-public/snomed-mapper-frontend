import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SnomedMapperComponent } from './snomed-mapper.component';
import { MatStepperModule } from '@angular/material/stepper';
import { RouterModule } from '@angular/router';
import { SnomedMapperRoutes } from './snomed-mapper.routes';
import { FileDropZoneComponent } from './components/csv-upload/file-drop-zone/file-drop-zone.component';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { FormsModule } from '@angular/forms';
import { DataMapperStepperComponent } from './components/data-mapper-stepper/data-mapper-stepper.component';
import { CsvDownloadComponent } from './components/csv-download/csv-download.component';
import { CsvUploadComponent } from './components/csv-upload/csv-upload.component';
import { SnomedMappingComponent } from './components/snomed-mapping/snomed-mapping.component';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import { HttpClientModule } from '@angular/common/http';
import { ColumnSelectionComponent } from './components/column-selection/column-selection.component';
import { MappingTableComponent } from './components/snomed-mapping/mapping-table/mapping-table.component';
import { MappingDialogComponent } from './components/snomed-mapping/mapping-dialog/mapping-dialog.component';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { MatDialogModule } from '@angular/material/dialog';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatDividerModule } from '@angular/material/divider';
import { SnomedMapperBackendService } from './services/snomed-mapper-backend.service';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

@NgModule({
  declarations: [
    SnomedMapperComponent,

    DataMapperStepperComponent,
    SnomedMappingComponent,
    CsvDownloadComponent,
    CsvUploadComponent,

    FileDropZoneComponent,
    ColumnSelectionComponent,
    MappingTableComponent,
    MappingDialogComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(SnomedMapperRoutes),
    HttpClientModule,

    MatStepperModule,
    MatIconModule,
    MatFormFieldModule,
    MatSelectModule,
    MatSlideToggleModule,
    FormsModule,
    MatButtonModule,
    MatTooltipModule,
    MatInputModule,
    MatTableModule,
    MatDialogModule,
    MatPaginatorModule,
    MatDividerModule,
    MatProgressSpinnerModule,
  ],
  providers: [SnomedMapperBackendService],
})
export class SnomedMapperModule {}
