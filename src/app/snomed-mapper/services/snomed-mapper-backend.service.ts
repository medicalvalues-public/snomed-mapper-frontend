import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { map, Observable } from 'rxjs';
import {
  RowStatus,
  SnomedSuggestion,
  TableData,
} from '../types/tabledata.interface';

export interface CSVUploadRequestData {
  file: File;
  headersIncluded: boolean;
  separator: string;
  language: string;
  semanticTag: string;
}

const URL = 'api';

@Injectable()
export class SnomedMapperBackendService {
  constructor(private http: HttpClient) {}

  uploadCsv(requestData: CSVUploadRequestData): Observable<string> {
    const formData = new FormData();
    formData.set('file', requestData.file);
    formData.set('fileName', requestData.file.name);
    formData.set('delimiter', requestData.separator);
    formData.set('language', requestData.language);
    formData.set(
      'headerIncluded',
      requestData.headersIncluded ? 'true' : 'false'
    );
    if (requestData.semanticTag.length) {
      formData.set('semanticTag', requestData.semanticTag);
    }
    return this.post<{ id: string }>('/upload/csv', formData).pipe(
      map((result) => result.id)
    );
  }

  getTableData(
    tableId: string,
    includeSuggestions: boolean,
    pagination: { count: number; offset: number }
  ): Observable<TableData> {
    const { count, offset } = pagination;
    const endpoint =
      `/dataset/${tableId}` + (includeSuggestions ? '/suggestions' : '');
    return this.get<TableData>(
      endpoint,
      new HttpParams().appendAll({ count, offset })
    );
  }

  setRelevantColumns(tableId: string, relevantColumnIndices: number[]) {
    return this.post(`/dataset/${tableId}/setRelevantColumns`, {
      relevantColumnIndices,
    });
  }

  searchSnomedByText(text: string, semanticTag: string | null) {
    const endpoint =
      `/search-by-text?query=${text}` +
      (semanticTag ? `&semanticTag=${semanticTag}` : '');
    return this.get<SnomedSuggestion[]>(endpoint);
  }

  updateRow(
    tableId: string,
    rowIndex: number,
    patch: { snomed: string; description: string; status: RowStatus }
  ) {
    const body = {
      rowIndex,
      ...patch,
    };
    return this.post(`/dataset/${tableId}/updateRow`, body);
  }

  downloadCsv(tableId: string): Observable<HttpResponse<Blob>> {
    const path = `/dataset/${tableId}/mappedCsv`;

    return this.http.get(URL + path, {
      observe: 'response',
      responseType: 'blob',
    });
  }

  private get<T>(
    endpoint: string,
    params: HttpParams | undefined = undefined
  ): Observable<T> {
    return this.http.get<T>(URL + endpoint, {
      params,
    });
  }

  private post<T>(endpoint: string, body: object | FormData): Observable<T> {
    return this.http.post<T>(URL + endpoint, body);
  }
}
