import { Injectable, OnDestroy } from '@angular/core';
import { SnomedMapperBackendService } from './snomed-mapper-backend.service';
import {
  BehaviorSubject,
  filter,
  firstValueFrom,
  Observable,
  Subscription,
} from 'rxjs';
import {
  RowStatus,
  SnomedSuggestion,
  TableData,
} from '../types/tabledata.interface';
import { notNullOrUndefined } from '../types/typeChecks/notNullOrUndefined';

@Injectable()
export class MapperService implements OnDestroy {
  private loading$ = new BehaviorSubject<boolean>(false);
  private tableData$ = new BehaviorSubject<TableData | null>(null);

  private subscription = new Subscription();
  constructor(private backendService: SnomedMapperBackendService) {}

  public loadRowsWithSuggestions(
    tableId: string,
    count: number,
    offset: number
  ) {
    this.loading$.next(true);

    this.subscription.add(
      this.backendService
        .getTableData(tableId, true, { count, offset })
        .subscribe((tableData) => {
          if (!tableData.headersIncluded) {
            tableData.columns.forEach((column, index) => {
              column.header = `Column ${index + 1}`;
            });
          }

          this.tableData$.next(tableData);
          this.loading$.next(false);
        })
    );
  }

  public isLoading(): Observable<boolean> {
    return this.loading$.asObservable();
  }

  public getTableData(): Observable<TableData> {
    return this.tableData$.asObservable().pipe(filter(notNullOrUndefined));
  }

  public async updateRow(
    rowIndex: number,
    status: RowStatus,
    snomed: SnomedSuggestion
  ) {
    const tableData = this.tableData$.value;
    if (!tableData) {
      return;
    }
    await firstValueFrom(
      this.backendService.updateRow(tableData.tableId, rowIndex, {
        ...snomed,
        status,
      })
    );
    const index = tableData.rows.findIndex((row) => row.rowIndex === rowIndex);
    if (index > -1) {
      tableData.rows[index] = {
        ...tableData.rows[index],
        snomed: snomed.snomed,
        snomedDescription: snomed.description,
        status,
      };

      this.tableData$.next(tableData);
    }
  }

  public ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
