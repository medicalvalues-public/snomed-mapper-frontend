import { TestBed } from '@angular/core/testing';

import { SnomedMapperBackendService } from './snomed-mapper-backend.service';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';

describe('SnomedMapperBackendService', () => {
  let service: SnomedMapperBackendService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [SnomedMapperBackendService],
    });
    service = TestBed.inject(SnomedMapperBackendService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('calls upload-endpoint to upload a file', (done) => {
    const file = { name: 'myFile' } as File;

    const tableId = 'myTableId';

    service
      .uploadCsv({
        file,
        headersIncluded: true,
        separator: ',',
        language: 'en',
        semanticTag: '',
      })
      .subscribe((tableId) => {
        expect(tableId).toEqual(tableId);
        done();
      });
    const mock = httpTestingController.expectOne('api/upload/csv');
    expect(mock.request.method).toEqual('POST');
    expect(mock.request.body.get('headerIncluded')).toEqual('true');
    expect(mock.request.body.get('delimiter')).toEqual(',');
    expect(mock.request.body.get('language')).toEqual('en');
    expect(mock.request.body.get('file')).toBeTruthy();
    mock.flush({ id: tableId });
  });
});
