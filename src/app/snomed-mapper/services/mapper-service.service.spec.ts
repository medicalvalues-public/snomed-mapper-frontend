import { TestBed } from '@angular/core/testing';
import { MapperService } from './mapper-service.service';
import { MockProvider } from 'ng-mocks';
import { SnomedMapperBackendService } from './snomed-mapper-backend.service';
import { firstValueFrom, ReplaySubject } from 'rxjs';
import { TableData } from '../types/tabledata.interface';

describe('MapperServiceService', () => {
  let service: MapperService;
  let tableDat$: ReplaySubject<TableData>;

  beforeEach(() => {
    tableDat$ = new ReplaySubject<TableData>();
    TestBed.configureTestingModule({
      providers: [
        MapperService,
        MockProvider(SnomedMapperBackendService, {
          getTableData: () => tableDat$.asObservable(),
        }),
      ],
    });
    service = TestBed.inject(MapperService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('loading TableData from the backend', () => {
    let tableData: TableData;
    beforeEach(() => {
      tableData = {
        filename: 'myData.csv',
        tableId: 'myTable',
        headersIncluded: true,
        language: 'en',
        rowCount: 2,
        columns: [
          { header: 'header1', containsDescription: true },
          { header: 'header2', containsDescription: false },
        ],
        rows: [],
      };
    });

    it('loads TableData', async () => {
      const spyOnBackend = jest.spyOn(
        TestBed.inject(SnomedMapperBackendService),
        'getTableData'
      );
      service.loadRowsWithSuggestions('tableId', 0, 20);
      tableDat$.next(tableData);
      expect(await firstValueFrom(service.getTableData())).toEqual(tableData);
      expect(spyOnBackend).toHaveBeenCalledWith('tableId', true, {
        count: 0,
        offset: 20,
      });
    });

    it('sets default headers if headers not included', async () => {
      tableData.headersIncluded = false;
      service.loadRowsWithSuggestions('tableId', 0, 20);
      tableDat$.next(tableData);
      const result = await firstValueFrom(service.getTableData());
      expect(result.columns.map((c) => c.header)).toEqual([
        'Column 1',
        'Column 2',
      ]);
    });

    it('sets isLoadingFlag while loading', async () => {
      service.loadRowsWithSuggestions('tableId', 0, 20);
      expect(await firstValueFrom(service.isLoading())).toEqual(true);
      tableDat$.next(tableData);
      expect(await firstValueFrom(service.isLoading())).toEqual(false);
    });
  });
});
