import { Component } from '@angular/core';
import { MappingStep, StepConfig } from './types/MappingSteps.enum';

@Component({
  selector: 'app-snomed-mapper',
  templateUrl: './snomed-mapper.component.html',
  styleUrls: ['./snomed-mapper.component.scss'],
})
export class SnomedMapperComponent {
  stepperConfig: StepConfig[] = [
    { page: MappingStep.CSV_UPLOAD, description: 'Upload your data file' },
    { page: MappingStep.COLUMN_SELECTION, description: 'Select columns' },
    { page: MappingStep.SNOMED_MAPPING, description: 'Map codes' },
    {
      page: MappingStep.CSV_DOWNLOAD,
      description: 'Download your mapped data file',
    },
  ];

  defaultPage = MappingStep.CSV_UPLOAD;
}
