import { Routes } from '@angular/router';
import { SnomedMapperComponent } from './snomed-mapper.component';
import { CsvUploadComponent } from './components/csv-upload/csv-upload.component';
import { MappingStep } from './types/MappingSteps.enum';
import { SnomedMappingComponent } from './components/snomed-mapping/snomed-mapping.component';
import { CsvDownloadComponent } from './components/csv-download/csv-download.component';
import { ColumnSelectionComponent } from './components/column-selection/column-selection.component';

export const SnomedMapperRoutes: Routes = [
  {
    path: '',
    component: SnomedMapperComponent,
    children: [
      {
        path: MappingStep.CSV_UPLOAD,
        component: CsvUploadComponent,
        data: {
          step: MappingStep.CSV_UPLOAD,
        },
      },
      {
        path: ':tableId/' + MappingStep.COLUMN_SELECTION,
        component: ColumnSelectionComponent,
        data: {
          step: MappingStep.COLUMN_SELECTION,
        },
      },
      {
        path: ':tableId/' + MappingStep.SNOMED_MAPPING,
        component: SnomedMappingComponent,
        data: {
          step: MappingStep.SNOMED_MAPPING,
        },
      },
      {
        path: ':tableId/' + MappingStep.CSV_DOWNLOAD,
        component: CsvDownloadComponent,
        data: {
          step: MappingStep.CSV_DOWNLOAD,
        },
      },
    ],
  },
];
