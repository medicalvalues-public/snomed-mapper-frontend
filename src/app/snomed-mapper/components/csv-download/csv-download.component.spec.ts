import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CsvDownloadComponent } from './csv-download.component';
import { RouterTestingModule } from '@angular/router/testing';
import { MockProvider } from 'ng-mocks';
import { SnomedMapperBackendService } from '../../services/snomed-mapper-backend.service';

describe('CsvDownloadComponent', () => {
  let component: CsvDownloadComponent;
  let fixture: ComponentFixture<CsvDownloadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CsvDownloadComponent],
      providers: [MockProvider(SnomedMapperBackendService)],
      imports: [RouterTestingModule],
    }).compileComponents();

    fixture = TestBed.createComponent(CsvDownloadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
