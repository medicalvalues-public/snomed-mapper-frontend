import { Component, OnDestroy } from '@angular/core';
import { saveAs } from 'file-saver';
import { HttpResponse } from '@angular/common/http';
import { filter, map, Subscription, switchMap } from 'rxjs';
import { notNullOrUndefined } from '../../types/typeChecks/notNullOrUndefined';
import { ActivatedRoute } from '@angular/router';
import { SnomedMapperBackendService } from '../../services/snomed-mapper-backend.service';

function saveResponse(response: HttpResponse<BlobPart>) {
  if (!response.body) {
    return;
  }
  const blob: Blob = new Blob([response.body], {
    type: 'text/csv; charset=utf-8',
  });
  const fileName =
    response.headers
      ?.getAll('Content-Disposition')?.[0]
      .split('filename=')[1] || 'mapped.csv';
  saveAs(blob, fileName);
}

@Component({
  selector: 'app-csv-download',
  templateUrl: './csv-download.component.html',
  styleUrls: ['./csv-download.component.scss'],
})
export class CsvDownloadComponent implements OnDestroy {
  isLoading = false;

  tableId$ = this.activatedRoute.paramMap.pipe(
    map((params) => params.get('tableId')),
    filter(notNullOrUndefined)
  );

  subscriptions = new Subscription();

  constructor(
    private activatedRoute: ActivatedRoute,
    private backendService: SnomedMapperBackendService
  ) {}

  downloadCsv() {
    this.isLoading = true;
    this.subscriptions.add(
      this.tableId$
        .pipe(
          switchMap((tableId: string) =>
            this.backendService.downloadCsv(tableId)
          )
        )
        .subscribe((response) => {
          saveResponse(response);
          this.isLoading = false;
        })
    );
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
