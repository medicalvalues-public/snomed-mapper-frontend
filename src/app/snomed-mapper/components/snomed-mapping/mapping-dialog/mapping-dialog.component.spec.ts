import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MappingDialogComponent } from './mapping-dialog.component';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MockProvider } from 'ng-mocks';
import { SnomedMapperBackendService } from '../../../services/snomed-mapper-backend.service';

describe('MappingDialogComponent', () => {
  let component: MappingDialogComponent;
  let fixture: ComponentFixture<MappingDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MappingDialogComponent],
      providers: [
        MockProvider(SnomedMapperBackendService),
        MockProvider(MatDialogRef),
        {
          provide: MAT_DIALOG_DATA,
          useValue: {
            columns: [],
            row: { suggestions: [] },
          },
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(MappingDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
