import { Component, Inject, Input, OnInit } from '@angular/core';
import {
  RowStatus,
  SnomedSuggestion,
  TableColumn,
  TableRow,
} from '../../../types/tabledata.interface';
import { BehaviorSubject, firstValueFrom } from 'rxjs';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { SnomedMapperBackendService } from '../../../services/snomed-mapper-backend.service';
import { SemanticTags } from '../../../types/semanticTags';

export interface MapperDialogData {
  columns: TableColumn[];
  row: TableRow;
}

export interface MapperDialogResult {
  snomed: SnomedSuggestion | null;
  status: RowStatus | null;
  openNext: boolean;
}

@Component({
  selector: 'app-mapping-dialog',
  templateUrl: './mapping-dialog.component.html',
  styleUrls: ['./mapping-dialog.component.scss'],
})
export class MappingDialogComponent implements OnInit {
  @Input()
  columns: TableColumn[] = [];

  @Input()
  row!: TableRow;

  semanticTags = SemanticTags;

  rowStatusEnum = RowStatus;

  searchQuery: string | null = null;
  searchSemanticTag: string | null = null;
  snomedSearching = false;

  selectedSnomed: SnomedSuggestion | null = null;
  status: RowStatus = RowStatus.MAPPED;

  snomeds: BehaviorSubject<SnomedSuggestion[]> = new BehaviorSubject<
    SnomedSuggestion[]
  >([]);

  constructor(
    @Inject(MAT_DIALOG_DATA) private data: MapperDialogData,
    private dialogRef: MatDialogRef<MappingDialogComponent, MapperDialogResult>,
    private backendService: SnomedMapperBackendService
  ) {}

  ngOnInit(): void {
    this.columns = this.data.columns;
    this.row = this.data.row;
    this.clearSearch();
    if (this.row.snomed && this.row.snomedDescription) {
      this.selectedSnomed = {
        snomed: this.row.snomed,
        description: this.row.snomedDescription,
      };
    }
  }

  public clearSearch() {
    this.searchQuery = null;
    this.snomeds.next(this.row.suggestions ?? []);
  }

  public async submitSearch() {
    if (!this.searchQuery) {
      this.clearSearch();
      return;
    }
    this.snomedSearching = true;
    const searchResult = await firstValueFrom(
      this.backendService.searchSnomedByText(
        this.searchQuery,
        this.searchSemanticTag
      )
    );
    this.snomeds.next(searchResult);
    this.snomedSearching = false;
  }

  clickOnRow(snomed: SnomedSuggestion) {
    this.selectedSnomed = snomed;
    this.status = RowStatus.MAPPED;
  }

  confirm(openNext: boolean) {
    this.dialogRef.close({
      snomed: this.selectedSnomed,
      status: this.status,
      openNext,
    });
  }

  skip() {
    this.status = RowStatus.SKIPPED;
    this.confirm(true);
  }
}
