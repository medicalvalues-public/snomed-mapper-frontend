import { Component, EventEmitter, Input, Output } from '@angular/core';
import {
  RowStatus,
  SnomedSuggestion,
  TableColumn,
  TableRow,
} from '../../../types/tabledata.interface';
import {
  MapperDialogData,
  MapperDialogResult,
  MappingDialogComponent,
} from '../mapping-dialog/mapping-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-mapping-table',
  templateUrl: './mapping-table.component.html',
  styleUrls: ['./mapping-table.component.scss'],
})
export class MappingTableComponent {
  @Input()
  columns: TableColumn[] = [];

  @Input()
  rows: TableRow[] = [];

  @Input() editable = true;

  @Output()
  updateRow = new EventEmitter<{
    rowIndex: number;
    status: RowStatus;
    snomed: SnomedSuggestion;
  }>();

  rowStatusEnum = RowStatus;

  constructor(private dialog: MatDialog) {}

  openSnomedSelectionDialog(row: TableRow) {
    const dialog = this.dialog.open<
      MappingDialogComponent,
      MapperDialogData,
      MapperDialogResult
    >(MappingDialogComponent, {
      height: '90vh',
      panelClass: 'custom-dialog-container',
      data: {
        columns: this.columns,
        row: row,
      },
    });

    dialog.afterClosed().subscribe((result) => {
      if (result && result.status && result.snomed) {
        this.updateRow.next({
          rowIndex: row.rowIndex,
          status: result.status,
          snomed: result.snomed,
        });
      }
      if (result?.openNext) {
        this.openNext(row.rowIndex);
      }
    });
  }

  openNext(rowIndex: number) {
    const nextRow = this.rows.find(
      (row) => row.status !== RowStatus.MAPPED && rowIndex < row.rowIndex
    );
    if (nextRow) {
      this.openSnomedSelectionDialog(nextRow);
    }
  }
}
