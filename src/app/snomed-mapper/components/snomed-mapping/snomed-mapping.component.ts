import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  BehaviorSubject,
  combineLatest,
  filter,
  map,
  Observable,
  Subscription,
} from 'rxjs';

import { ActivatedRoute, Router } from '@angular/router';
import { MapperService } from '../../services/mapper-service.service';
import { PageEvent } from '@angular/material/paginator';
import {
  RowStatus,
  SnomedSuggestion,
  TableData,
} from '../../types/tabledata.interface';
import { notNullOrUndefined } from '../../types/typeChecks/notNullOrUndefined';
import { MappingStep } from '../../types/MappingSteps.enum';

@Component({
  selector: 'app-snomed-mapping',
  templateUrl: './snomed-mapping.component.html',
  styleUrls: ['./snomed-mapping.component.scss'],
  providers: [MapperService],
})
export class SnomedMappingComponent implements OnInit, OnDestroy {
  pageSize = 20;

  tableId$ = this.activatedRoute.paramMap.pipe(
    map((params) => params.get('tableId')),
    filter(notNullOrUndefined)
  );

  tableData$: Observable<TableData> = this.mapperService.getTableData();
  rowCount$ = this.tableData$.pipe(map((tableData) => tableData.rowCount));
  isLoading$ = this.mapperService.isLoading();

  private pageOffset$ = new BehaviorSubject<number>(0);

  private subscription = new Subscription();

  constructor(
    private mapperService: MapperService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.subscription.add(
      combineLatest([this.tableId$, this.pageOffset$]).subscribe(
        ([tableId, pageOffset]) => {
          this.mapperService.loadRowsWithSuggestions(
            tableId,
            this.pageSize,
            pageOffset
          );
        }
      )
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  paginatorUpdate(event: PageEvent) {
    this.pageOffset$.next(event.pageIndex * this.pageSize);
  }

  async updateRow({
    rowIndex,
    status,
    snomed,
  }: {
    rowIndex: number;
    status: RowStatus;
    snomed: SnomedSuggestion;
  }) {
    await this.mapperService.updateRow(rowIndex, status, snomed);
  }

  submit() {
    this.tableId$.subscribe((tableId) =>
      this.router.navigate([tableId, MappingStep.CSV_DOWNLOAD])
    );
  }
}
