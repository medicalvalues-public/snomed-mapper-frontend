import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SnomedMappingComponent } from './snomed-mapping.component';
import { MockComponents, MockProvider } from 'ng-mocks';
import { SnomedMapperBackendService } from '../../services/snomed-mapper-backend.service';
import { RouterTestingModule } from '@angular/router/testing';
import { firstValueFrom, of, ReplaySubject } from 'rxjs';
import { TableData } from '../../types/tabledata.interface';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatDivider } from '@angular/material/divider';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
import { MapperService } from '../../services/mapper-service.service';

const tableId = 'tableId';

describe('SnomedMappingComponent', () => {
  let component: SnomedMappingComponent;
  let fixture: ComponentFixture<SnomedMappingComponent>;
  let tableData$: ReplaySubject<TableData>;

  beforeEach(async () => {
    tableData$ = new ReplaySubject<TableData>();

    await TestBed.configureTestingModule({
      declarations: [
        SnomedMappingComponent,
        MockComponents(MatPaginator, MatDivider, MatProgressSpinner),
      ],
      providers: [MapperService, MockProvider(SnomedMapperBackendService)],
      imports: [RouterTestingModule],
    }).compileComponents();

    jest
      .spyOn(TestBed.inject(SnomedMapperBackendService), 'getTableData')
      .mockReturnValue(tableData$.asObservable());

    jest
      .spyOn(TestBed.inject(ActivatedRoute), 'paramMap', 'get')
      .mockReturnValue(of(convertToParamMap({ tableId })));

    fixture = TestBed.createComponent(SnomedMappingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('loads tableData from mapperService', async () => {
    const tableDataOnComponent = firstValueFrom(component.tableData$);

    const tableData: TableData = {
      filename: 'myData.csv',
      tableId: 'myTableId',
      headersIncluded: true,
      language: 'en',
      rowCount: 2,
      columns: [
        { header: 'header1', containsDescription: true },
        { header: 'header2', containsDescription: false },
      ],
      rows: [],
    };
    tableData$.next(tableData);

    expect(await tableDataOnComponent).toEqual(tableData);
  });

  it('reloadsData if pagination changes', async () => {
    const spyOnReload = jest.spyOn(
      TestBed.inject(SnomedMapperBackendService),
      'getTableData'
    );

    component.paginatorUpdate({ pageIndex: 2 } as PageEvent);
    expect(spyOnReload).toHaveBeenCalledWith(tableId, true, {
      count: component.pageSize,
      offset: component.pageSize * 2,
    });
  });
});
