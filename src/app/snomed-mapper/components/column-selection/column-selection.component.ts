import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { filter, firstValueFrom, map, Subscription, switchMap } from 'rxjs';
import { SnomedMapperBackendService } from '../../services/snomed-mapper-backend.service';
import { notNullOrUndefined } from '../../types/typeChecks/notNullOrUndefined';
import { MappingStep } from '../../types/MappingSteps.enum';

@Component({
  selector: 'app-column-selection',
  templateUrl: './column-selection.component.html',
  styleUrls: ['./column-selection.component.scss'],
})
export class ColumnSelectionComponent implements OnInit, OnDestroy {
  tableId$ = this.activatedRoute.paramMap.pipe(
    map((params) => params.get('tableId')),
    filter(notNullOrUndefined)
  );

  selectedColumns: Set<number> = new Set();

  tableData$ = this.tableId$.pipe(
    switchMap((tableId) =>
      this.backendService.getTableData(tableId, false, { offset: 0, count: 50 })
    )
  );

  private subscription = new Subscription();

  constructor(
    private backendService: SnomedMapperBackendService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.subscription.add(
      this.tableData$.subscribe((tableData) => {
        this.selectedColumns = new Set(
          tableData.columns
            .map((column, index) => ({
              containsDescription: column.containsDescription,
              index,
            }))
            .filter(({ containsDescription }) => containsDescription)
            .map(({ index }) => index)
        );
      })
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  toggleColumnSelection(index: number) {
    if (this.selectedColumns.has(index)) {
      this.selectedColumns.delete(index);
    } else {
      this.selectedColumns.add(index);
    }
  }

  async submit() {
    const tableId = await firstValueFrom(this.tableId$);
    this.backendService
      .setRelevantColumns(tableId, Array.from(this.selectedColumns))
      .subscribe(() =>
        this.router.navigate([tableId, MappingStep.SNOMED_MAPPING])
      );
  }
}
