import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ColumnSelectionComponent } from './column-selection.component';
import { MockProvider } from 'ng-mocks';
import { SnomedMapperBackendService } from '../../services/snomed-mapper-backend.service';
import { RouterTestingModule } from '@angular/router/testing';

describe('HeaderSelectionComponent', () => {
  let component: ColumnSelectionComponent;
  let fixture: ComponentFixture<ColumnSelectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      providers: [MockProvider(SnomedMapperBackendService)],
      declarations: [ColumnSelectionComponent],
      imports: [RouterTestingModule],
    }).compileComponents();

    fixture = TestBed.createComponent(ColumnSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
