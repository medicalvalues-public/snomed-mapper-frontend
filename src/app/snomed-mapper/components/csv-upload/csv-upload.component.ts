import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { SnomedMapperBackendService } from '../../services/snomed-mapper-backend.service';
import { Router } from '@angular/router';
import { MappingStep } from '../../types/MappingSteps.enum';
import { SemanticTags } from '../../types/semanticTags';

const Separators = Object.freeze({
  COMMA: {
    description: 'Comma',
    char: ',',
  },
  SEMICOLON: {
    description: 'Semicolon',
    char: ';',
  },
  TAB: {
    description: 'Tab',
    char: '\t',
  },
});

@Component({
  selector: 'app-csv-upload',
  templateUrl: './csv-upload.component.html',
  styleUrls: ['./csv-upload.component.scss'],
})
export class CsvUploadComponent implements OnDestroy {
  availableSeparators = Object.values(Separators);

  file: File | undefined = undefined;

  headersIncluded = false;
  selectedSeparator = Separators.SEMICOLON;

  errorMessage: string | null = null;

  subscription: Subscription = new Subscription();
  semanticTag = '';

  semanticTags = SemanticTags;

  constructor(
    private backend: SnomedMapperBackendService,
    private router: Router
  ) {}

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  public uploadFile() {
    if (!this.file) {
      return;
    }
    this.subscription.add(
      this.backend
        .uploadCsv({
          file: this.file,
          headersIncluded: this.headersIncluded,
          language: 'en',
          separator: this.selectedSeparator.char,
          semanticTag: this.semanticTag,
        })
        .subscribe((tableId) =>
          this.router.navigate([tableId, MappingStep.COLUMN_SELECTION])
        )
    );
  }
}
