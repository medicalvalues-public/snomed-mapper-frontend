import {
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  Output,
  ViewChild,
} from '@angular/core';

@Component({
  selector: 'app-file-drop-zone',
  templateUrl: './file-drop-zone.component.html',
  styleUrls: ['./file-drop-zone.component.scss'],
})
export class FileDropZoneComponent {
  @ViewChild('fileInput') fileInputElement: ElementRef | undefined;

  @Input() accept = 'text/csv';
  @Input() file: File | undefined;

  @Output() fileChange = new EventEmitter<File>();

  isFileDragged = false;
  isFileCsv = false;
  fileSizeText = 'unknown';

  constructor() {
    this.isFileCsv = !!(
      this.file && this.file?.name.toLowerCase().endsWith('.csv')
    );
  }

  @HostListener('dragenter', ['$event'])
  onDragEnter(event: DragEvent): void {
    event.stopPropagation();
    this.isFileDragged = true;
  }

  @HostListener('dragleave', ['$event'])
  @HostListener('drop', ['$event'])
  onDragLeave(event: DragEvent): void {
    event.stopPropagation();
    this.isFileDragged = false;
  }

  fileDropped(event: DragEvent) {
    if (event.target) {
      event.preventDefault();
      event.stopPropagation();
      this.chooseFile(event.dataTransfer?.files);
      this.isFileDragged = false;
    }
  }

  fileSelected(event: Event) {
    const input = <HTMLInputElement>event.target;
    if (input.files) {
      this.chooseFile(input.files);
    }
  }

  chooseFile(files: FileList | null | undefined) {
    if (!files || files.length !== 1) {
      return;
    }
    this.setFileSizeText(files[0]?.size);
    this.isFileCsv = files[0].name.toLowerCase().endsWith('.csv');
    this.fileChange.emit(files[0]);
  }

  setFileSizeText(valueInByte?: number | undefined): void {
    if (valueInByte) {
      if (valueInByte < 1000) {
        this.fileSizeText = `${valueInByte} Byte`;
      } else if (valueInByte < 1000000) {
        this.fileSizeText = `${Math.round(valueInByte / 1000)} Kilobyte`;
      } else {
        this.fileSizeText = `${Math.round(valueInByte / 1000 ** 2)} Megabyte`;
      }
    } else {
      this.fileSizeText = '< 1 Byte';
    }
  }

  deleteFile(): void {
    this.file = undefined;
    this.isFileCsv = false;
    this.setFileSizeText();
    this.fileChange.emit();
  }
}
