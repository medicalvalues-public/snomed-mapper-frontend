import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CsvUploadComponent } from './csv-upload.component';
import { FileDropZoneComponent } from './file-drop-zone/file-drop-zone.component';
import { RouterTestingModule } from '@angular/router/testing';
import { MockComponents, MockProvider } from 'ng-mocks';
import { SnomedMapperBackendService } from '../../services/snomed-mapper-backend.service';
import { of } from 'rxjs';
import { Router } from '@angular/router';
import { MappingStep } from '../../types/MappingSteps.enum';
import { MatIconModule } from '@angular/material/icon';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import SpyInstance = jest.SpyInstance;

describe('CsvUploadComponent', () => {
  let component: CsvUploadComponent;
  let fixture: ComponentFixture<CsvUploadComponent>;
  let backendService: SnomedMapperBackendService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MatIconModule,
        MatFormFieldModule,
        MatSelectModule,
        MatSlideToggleModule,
        FormsModule,
        MatButtonModule,
        MatTooltipModule,
        NoopAnimationsModule,
      ],
      declarations: [CsvUploadComponent, MockComponents(FileDropZoneComponent)],
      providers: [MockProvider(SnomedMapperBackendService)],
    }).compileComponents();

    backendService = TestBed.inject(SnomedMapperBackendService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CsvUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('upload file', () => {
    const tableId = 'myTableId';
    const file = { name: 'myFile' } as File;
    let spyOnNavigate: SpyInstance;
    let spyOnUpload: SpyInstance;

    beforeEach(() => {
      spyOnNavigate = jest
        .spyOn(TestBed.inject(Router), 'navigate')
        .mockResolvedValue(true);
      spyOnUpload = jest
        .spyOn(backendService, 'uploadCsv')
        .mockReturnValue(of(tableId));

      component.file = file;
      component.uploadFile();
    });

    it('should call Backend service to upload file', () => {
      expect(spyOnUpload).toHaveBeenCalledWith(
        expect.objectContaining({ file })
      );
    });

    it('should navigate to next page', () => {
      expect(spyOnNavigate).toHaveBeenCalledWith([
        tableId,
        MappingStep.COLUMN_SELECTION,
      ]);
    });
  });
});
