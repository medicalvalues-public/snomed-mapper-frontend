import {
  ComponentFixture,
  fakeAsync,
  flush,
  TestBed,
} from '@angular/core/testing';
import { DataMapperStepperComponent } from './data-mapper-stepper.component';
import {
  ActivatedRoute,
  ActivatedRouteSnapshot,
  NavigationEnd,
  Router,
} from '@angular/router';
import { Subject } from 'rxjs';
import { MatStepperModule } from '@angular/material/stepper';

import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MappingStep } from '../../types/MappingSteps.enum';

const stepperConfig = [
  { page: MappingStep.CSV_UPLOAD, description: 'Upload your .csv file' },
  { page: MappingStep.COLUMN_SELECTION, description: 'Match column headers' },
];

describe('DataMapperStepperComponent', () => {
  let component: DataMapperStepperComponent;
  let fixture: ComponentFixture<DataMapperStepperComponent>;
  let routerEvents: Subject<NavigationEnd>;
  let snapshot: { firstChild: Partial<ActivatedRouteSnapshot['firstChild']> };

  beforeEach(async () => {
    snapshot = {} as ActivatedRouteSnapshot;
    routerEvents = new Subject<NavigationEnd>();
    await TestBed.configureTestingModule({
      declarations: [DataMapperStepperComponent],
      providers: [
        { provide: ActivatedRoute, useValue: { snapshot } },
        { provide: Router, useValue: { events: routerEvents.asObservable() } },
      ],
      imports: [MatStepperModule, NoopAnimationsModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataMapperStepperComponent);
    component = fixture.componentInstance;
    component.steps = stepperConfig;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set stepper to correct step ', fakeAsync(() => {
    snapshot.firstChild = {
      params: { tableId: '123456789' },
      data: { step: MappingStep.COLUMN_SELECTION },
    };
    routerEvents.next(new NavigationEnd(123, 'url', 'urlAfterRedirect'));
    flush();
    expect(component.stepper.selectedIndex).toEqual(1);
  }));
});
