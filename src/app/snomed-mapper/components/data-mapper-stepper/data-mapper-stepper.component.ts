import {
  AfterViewInit,
  Component,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';

import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { MatStepper } from '@angular/material/stepper';
import { Subscription } from 'rxjs';

import { MappingStep, StepConfig } from '../../types/MappingSteps.enum';

@Component({
  selector: 'app-data-mapper-stepper',
  templateUrl: './data-mapper-stepper.component.html',
  styleUrls: ['./data-mapper-stepper.component.scss'],
})
export class DataMapperStepperComponent
  implements OnInit, AfterViewInit, OnDestroy
{
  @Input()
  steps: StepConfig[] = [];

  @Input()
  defaultPage: MappingStep = MappingStep.CSV_UPLOAD;

  @ViewChild(MatStepper, { static: true })
  stepper!: MatStepper;

  subscription = new Subscription();

  constructor(private router: Router, private activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    const routerSubscription = this.router.events.subscribe(async (event) => {
      if (event instanceof NavigationEnd) {
        await this.updateStepperFromRoute();
      }
    });
    this.subscription.add(routerSubscription);
  }

  ngAfterViewInit() {
    setTimeout(async () => {
      await this.updateStepperFromRoute();
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private async updateStepperFromRoute() {
    const childRouteSnapshot = this.activatedRoute.snapshot.firstChild;
    if (!childRouteSnapshot) {
      await this.router.navigate([MappingStep.CSV_UPLOAD], {
        relativeTo: this.activatedRoute.parent,
      });
      return;
    }

    const tableId = childRouteSnapshot?.params['tableId'];
    const page = tableId ? childRouteSnapshot.data['step'] : this.defaultPage;
    if (page && this.steps.some((step) => step.page === page)) {
      this.setStepper(page as MappingStep);
    } else {
      await this.router.navigate([this.defaultPage], {
        relativeTo: this.activatedRoute.parent,
      });
    }
  }

  private setStepper(currentPage: MappingStep) {
    this.stepper.reset();
    const activePageIndex =
      this.steps.findIndex((step) => currentPage === step.page) ?? 0;
    // ugly way to set the stepper to the correct position
    for (let i = 0; i < activePageIndex; i++) {
      this.stepper.next();
    }
  }
}
