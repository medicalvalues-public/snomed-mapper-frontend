import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppShellComponent } from './app-shell.component';
import { AppHeaderComponent } from './app-header/app-header.component';

@NgModule({
  declarations: [AppShellComponent, AppHeaderComponent],
  imports: [CommonModule],
  exports: [AppShellComponent],
})
export class AppShellModule {}
