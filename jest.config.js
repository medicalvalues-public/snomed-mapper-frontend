module.exports = {
  preset: "jest-preset-angular",
  testPathIgnorePatterns: ["/testing/e2e/", "/node_modules"],
  roots: ["<rootDir>/src/"],
  testMatch: ["**/+(*.)+(spec).+(ts)"],
  setupFilesAfterEnv: ["<rootDir>/src/test.ts"],
  collectCoverage: true,
  coverageReporters: ["html", "lcov"],
  coverageDirectory: "coverage",
  modulePaths: ["node_modules", "<rootDir>"],
  testResultsProcessor: "jest-sonar-reporter",
};
