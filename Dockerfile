# =====
# Building Stage
FROM node:18-alpine AS building

ARG NPM_REGISTRY_URL
ARG NPM_REGISTRY_TOKEN

WORKDIR /app
COPY package.json package-lock.json ./
RUN echo "@medicalvalues:registry=${NPM_REGISTRY_URL}" >> .npmrc
RUN echo "${NPM_REGISTRY_URL#https?}:_authToken=${NPM_REGISTRY_TOKEN}" >> .npmrc
RUN npm ci

COPY src ./src
COPY tailwind.config.js angular.json tsconfig.json tsconfig.app.json tsconfig.spec.json .browserslistrc ./
RUN npm run build -- --base-href "<!--#echo var='base_href' -->/"

# =====
# Application Stage
FROM nginx:1.21-alpine

ARG VERSION=unspecified
LABEL Name="snomed_mapper_frontend"
LABEL Version="${VERSION}"

COPY --from=building /app/dist/snomed-mapper-frontend/ /usr/share/nginx/html/

COPY default.nginx.conf /etc/nginx/conf.d/default.conf
COPY security-headers.nginx.conf /etc/nginx/security-headers.conf

EXPOSE 80
